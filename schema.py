from graphene import ObjectType, String, Boolean, ID, List, Field, Int
import requests as req
import requests_cache
import json
import os
from collections import namedtuple

requests_cache.install_cache('starwars_cache', backend='sqlite', expire_after=180)

def __json_object_hook(d):
    return namedtuple('X', d.keys())(*d.values())

def json2obj(data):
    return json.loads(data, object_hook=__json_object_hook)

def get_content(url):
    resp = req.get(url)
    return json2obj(resp.content)

class Film(ObjectType):
    title = String()
    episode_id = Int()
    opening_crawl = String()
    director = String()
    producer = String()
    release_date = String()
    characters = List(lambda: Person)
    planets = List(lambda: Planet)
    starships = List(lambda: Starship)
    species = List(lambda: Species)

    def resolve_characters(self, info):
        return list(map(get_content, self.characters))

    def resolve_planets(self, info):
        return list(map(get_content, self.planets))

    def resolve_starships(self, info):
        return list(map(get_content, self.starships))

    def resolve_species(self, info):
        return list(map(get_content, self.species))

class Planet(ObjectType):
    name = String()
    rotation_period = String()
    population = String()
    residents = List(lambda: Person)

    def resolve_residents(self, info):
        return list(map(get_content, self.residents))

class Species(ObjectType):
    name = String()
    classification = String()
    designation = String()
    average_height = String()
    homeworld = Field(lambda: Planet)
    language = String()
    people = List(lambda: Person)
    films = List(lambda: Film)

    def resolve_homeworld(self, info):
        return get_content(self.homeworld)

    def resolve_people(self, info):
        return list(map(get_content, self.people))

    def resolve_films(self, info):
        return list(map(get_content, self.films))

class Starship(ObjectType):
    name = String()
    model = String()
    manufacturer = String()
    cargo_capacity = String()
    pilots = List(lambda: Person)
    films = List(lambda: Film)

    def resolve_pilots(self, info):
        return list(map(get_content, self.pilots))

    def resolve_films(self, info):
        return list(map(get_content, self.films))

class Person(ObjectType):
    name = String()
    height = String()
    mass = String()
    hair_color = String()
    skin_color = String()
    eye_color = String()
    birth_year = String()
    gender = String()
    homeworld = Field(lambda: Planet)
    films = List(lambda: Film)
    starships = List(lambda: Starship)
    species = List(lambda: Species)

    def resolve_films(self, info):
        return list(map(get_content, self.films))

    def resolve_homeworld(self, info):
        return get_content(self.homeworld)
    
    def resolve_starships(self, info):
        return list(map(get_content, self.starships))

    def resolve_species(self, info):
        return list(map(get_content, self.species))

class Query(ObjectType):
    person = Field(Person, id=ID(required=True))
    people = List(Person, page=Int(default_value=1))
    films = List(Film, page=Int(default_value=1))

    def resolve_person(self, info, id):
        resp = req.get(f"https://swapi.dev/api/people/{id}")
        return json2obj(resp.content)

    def resolve_people(self, info, page):
        resp = req.get(f"https://swapi.dev/api/people/?page={page}")
        return json2obj(resp.content).results

    def resolve_films(self, info, page):
        resp = req.get(f"https://swapi.dev/api/films/?page={page}")
        return json2obj(resp.content).results