# Swagt

starwars api graphql test (swagt)

## step 1

install python 3.6+
```
apt update && upgrade
apt install python3 python3-pip ipython3
```
install pipenv
```
pip install pipenv
```
## step 2

install dependencies
```
pipenv install
```

## step 3

start virtualenv
```
pipenv shell
```

## step 4

run server
```
pipenv run python3 server.py
```

## step 5

```
http://localhost:5000/graphql
```

## step 6
```
{
    people (page: 1) {
        name
        homeworld {
            name
        }
        starships {
            name
        }
    }
}
```
